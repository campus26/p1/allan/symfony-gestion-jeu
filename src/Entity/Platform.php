<?php

namespace App\Entity;

use App\Repository\PlatformRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlatformRepository::class)
 */
class Platform
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name_plat;

    /**
     * @ORM\Column(type="text")
     */
    private $description_plat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logo_plat;

    /**
     * @ORM\ManyToMany(targetEntity=Game::class, mappedBy="platform")
     */
    private $game;

    /**
     * @ORM\ManyToMany(targetEntity=UserHasGame::class, mappedBy="platform")
     */
    private $user_has_game;

    public function __construct()
    {
        $this->game = new ArrayCollection();
        $this->user_has_game = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNamePlat(): ?string
    {
        return $this->name_plat;
    }

    public function setNamePlat(string $name_plat): self
    {
        $this->name_plat = $name_plat;

        return $this;
    }

    public function getDescriptionPlat(): ?string
    {
        return $this->description_plat;
    }

    public function setDescriptionPlat(string $description_plat): self
    {
        $this->description_plat = $description_plat;

        return $this;
    }

    public function getLogoPlat(): ?string
    {
        return $this->logo_plat;
    }

    public function setLogoPlat(?string $logo_plat): self
    {
        $this->logo_plat = $logo_plat;

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGame(): Collection
    {
        return $this->game;
    }

    public function addGame(Game $game): self
    {
        if (!$this->game->contains($game)) {
            $this->game[] = $game;
            $game->addPlatform($this);
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->game->removeElement($game)) {
            $game->removePlatform($this);
        }

        return $this;
    }

    /**
     * @return Collection|userhasgame[]
     */
    public function getUserHasGame(): Collection
    {
        return $this->user_has_game;
    }

    public function addUserHasGame(userhasgame $userHasGame): self
    {
        if (!$this->user_has_game->contains($userHasGame)) {
            $this->user_has_game[] = $userHasGame;
        }

        return $this;
    }

    public function removeUserHasGame(userhasgame $userHasGame): self
    {
        $this->user_has_game->removeElement($userHasGame);

        return $this;
    }
}
